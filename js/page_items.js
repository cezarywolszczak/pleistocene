function refresh() {
    var data = new Date();
    var y = data.getFullYear();
    var month = data.getMonth() + 1;
    if (month < 10) {
        month = "0" + month;
    }
    var day = data.getDate();
    if (day < 10) {
        day = "0" + day;
    }
    var h = data.getHours();
    if (h < 10) {
        h = "0" + h;
    }
    var min = data.getMinutes();
    if (min < 10) {
        min = "0" + min;
    }
    var sek = data.getSeconds();
    if (sek < 10) {
        sek = "0" + sek;
    }
    document.getElementById("timer").innerHTML = day + "." + month + "." + y + " " + h + ":" + min + ":" + sek;
    setTimeout("refresh()", 1000);
}

function tooltips(className){
    $(function() {
        $(className).tooltip({
            show: "slideDown",
            content: function(callback) {
                callback($(this).prop('title').replace('|', '<br />'));
            }
        });
    });
}

//function initial_progres_bar(time_to_end){
    // var progress3 = $(".loading-progress-3").progressTimer({
    //     timeLimit: time_to_end
    // });
    // $.ajax({
    //     url: "https://api.github.com/users/octocat/orgs"
    // }).error(function () {
    //     progress3.progressTimer('error', {
    //         errorText: 'Cannot connect to github api',
    //         onFinish: function () {
    //             console.log('3 - Cannot connect to github api');
    //         }
    //     });
    // }).done(function () {
    //     progress3.progressTimer('complete', {
    //         successText: 'Connected successfully to github api',
    //         onFinish: function () {
    //             var successText = '3 - Connected successfully to github api';
    //             console.log(successText);
    //             var glyph = $('<span></span>').addClass('glyphicon glyphicon-ok');
    //             $(".loading-progress-3").append($('<p></p>').append(glyph).append(' ' + successText));
    //         }
    //     });
    // });
//}
function init_progress_bar(timeTotal, startTime, endTime){
    var timer = 0,
        perc = 0,
        timeCount = 1,
        cFlag;

    timeTotal = timeTotal * 100;

    function updatePercentage(){

    }

    function updateProgress(percentage) {
        var x = (percentage/timeTotal)*100,
            y = x.toFixed(0);
        $('.pbar_innerdiv').css("width", x + "%");
        $('.pbar_innertext').text(y + "%");
    }

    function animateUpdate() {
        if(perc < timeTotal) {
            perc++;
            updateProgress(perc);
            timer = setTimeout(animateUpdate, timeCount); 
        }else{
            $('.pbar_innerdiv').css("opacity", 0);
            $('.pbar_innerdiv').css("transition", "all 1s");
            $('.pbar_innertext').css("opacity", 0);      
        }
    }

    $(document).ready(function() {
        $('.materials_level_up_button').click(function() {
            if (cFlag == undefined) {     
                clearTimeout(timer);
                perc = 0;
                cFlag = true;
                animateUpdate();
            }
            else if (!cFlag) {
                cFlag = true;
                animateUpdate();
            }
            else {
                clearTimeout(timer);
                cFlag = false;
            }
        });
    }); 
}
