<?php
require_once '../game/php_class/Game_container.php';
require_once '../game/php_class/Kernel.php';
require_once 'connect.php';
session_start();

$kernel = new Kernel;
$kernel->send_to_base($host,$db_user,$db_password,$db_name);

session_unset();
header('Location: ../index.php');

?>