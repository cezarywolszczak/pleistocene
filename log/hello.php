<?php
session_start();
if (!isset($_SESSION['registration_complete'])) {
    header('Location: ../index.php');
    exit();
} else {
    unset($_SESSION['registration_complete']);
}

if (isset($_SESSION['fr_nick']))
    unset($_SESSION['fr_nick']);
if (isset($_SESSION['fr_email']))
    unset($_SESSION['fr_email']);
if (isset($_SESSION['fr_password']))
    unset($_SESSION['fr_password']);
if (isset($_SESSION['fr_password_repeat']))
    unset($_SESSION['fr_password_repeat']);
if (isset($_SESSION['fr_password_repeat']))
    unset($_SESSION['fr_password_repeat']);

if (isset($_SESSION['e_nick']))
    unset($_SESSION['e_nick']);
if (isset($_SESSION['e_email']))
    unset($_SESSION['e_email']);
if (isset($_SESSION['e_password']))
    unset($_SESSION['e_password']);
if (isset($_SESSION['e_password_repeat']))
    unset($_SESSION['e_password_repeat']);
if (isset($_SESSION['e_regulations']))
    unset($_SESSION['e_regulations']);

?>
<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <meta name="discriptions" content="">
    <meta name="author" content="Cezary Wolszczak">
    <meta name="keywords" content="">
    <meta http-equiv="x-ua-compatible" content="IE=edge"/>
    <title>Plejstocen - Gra przeglądarkowa</title>
    <link rel="icon" href="">


    <script src="" type="text/javascript"></script>
    <link rel="stylesheet" href="../css/style.css" type="text/css"/>
    <link rel="stylesheet" href="../css/log_register.css" type="text/css"/>

</head>
<body>
Dziękuje za rejestracje.
<a href="../index.php">Strona logowania</a>
</body>

</html>