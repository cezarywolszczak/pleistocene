<?php
session_start();

if (isset($_POST['email'])) {

    $validation = true;

    //validation of nick
    @$nick = $_POST['nick'];
    if ((strlen($nick) < 3) || (strlen($nick) > 23)) {
        $validation = false;
        $_SESSION['e_nick'] = "Nick musi posiadać od 3 do 23 znaków!";
    }
    if (ctype_alnum($nick) == false) { //sprawdzenie czy wszystkie znaki są alfanumeryczne
        $validation = false;
        $_SESSION['e_nick'] = 'Nick może składać się wyłącznie z liter i cyfr (bez polskich znaków)!';
    }

    //validation of e-mail
    $email = $_POST['email'];
    $email_sanit = filter_var($email, FILTER_SANITIZE_EMAIL);
    if ((filter_var($email_sanit, FILTER_VALIDATE_EMAIL) == false) || ($email_sanit != $email)) { //filter_var - sprawdzenie i validacja emaila
        $validation = false;
        $_SESSION['e_email'] = 'Podaj poprawny adres e-mail';
    }

    //validation of password
    $password = $_POST['password'];
    $password_repeat = $_POST['password_repeat'];
    if ((strlen($password) < 8) || (strlen($password) > 26)) {
        $validation = false;
        $_SESSION['e_password'] = "Hasło musi mieć od 8 do 26 znaków";
    }
    if ($password != $password_repeat) {
        $validation = false;
        $_SESSION['e_password_repeat'] = "Podane hasła nie są identyczne";
    }
    @$password_hash = password_hash($password, PASSWORD_DEFAULT);

    //validation checkbox, regulations
    if (!isset($_POST['regulations'])) {
        $validation = false;
        $_SESSION['e_regulations'] = "Potwierdź kaceptacje regulaminu";
    }

    //validation reCaptcha
    $secret_key = "6Ldo-hYTAAAAAE5KBEacRKC81A1FBEcPaV54VGvs";
    $check = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . $secret_key . '&response=' . $_POST['g-recaptcha-response']);
    $response = json_decode($check);
    if (!$response->success) {
        $validation = false;
        $_SESSION['e_recaptcha'] = "Potwierdź że nie jesteś botem!";
    }

    //remember data_in
    $_SESSION['fr_nick'] = $nick;
    $_SESSION['fr_email'] = $email;
    $_SESSION['fr_password'] = $password;
    $_SESSION['fr_password_repeat'] = $password_repeat;
    if (isset($_POST['regulations']))
        $_SESSION['fr_regulations'] = true;

    require_once "connect.php";
    mysqli_report(MYSQLI_REPORT_STRICT); //raportowanie błędów oparte o wyjątki
    try {
        $conection = new mysqli($host, $db_user, $db_password, $db_name);
        if ($conection->connect_errno != 0) {
            throw new Exception(mysqli_connect_errno());
        } else {
            //is email exist
            $result = $conection->query("SELECT id FROM users WHERE email='$email'");
            if (!$result)
                throw new Exception($conection->error);
            $number_of_mail = $result->num_rows;
            if ($number_of_mail > 0) {
                $validation = false;
                $_SESSION['e_email'] = "Podany email już istnieje";
            }

            //is nick exist
            $result = $conection->query("SELECT id FROM users WHERE user='$nick'");
            if (!$result)
                throw new Exception($conection->error);
            $number_of_nisks = $result->num_rows;
            if ($number_of_nisks > 0) {
                $validation = false;
                $_SESSION['e_nick'] = "Podany nick już istnieje";
            }

            if ($validation == true) {
                //$actual_date = date("Y-m-d h:i:s");
                $actual_date = time();
                if ($conection->query("INSERT INTO users VALUES (NULL, '$nick','$password_hash','$email',100,100,100,100,100,1,'$actual_date')")) {
                    $_SESSION['registration_complete'] = true;
                    header("Location: hello.php");
                } else {
                    throw new Exception($conection->error);
                }

            }
            $conection->close();
        }
    } catch (Exception $e) {
        echo "Błąd serwera prosimy zarejestrować się w innym terminie<br>";
        echo "Info o błędzie: " . $e;
    }

}
?>
<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <meta name="discriptions" content="">
    <meta name="author" content="Cezary Wolszczak">
    <meta name="keywords" content="">
    <meta http-equiv="x-ua-compatible" content="IE=edge"/>
    <title>Plejstocen - Załóż konto</title>
    <link rel="icon" href="">

    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script src="" type="text/javascript"></script>
    <link rel="stylesheet" href="../css/style.css" type="text/css"/>
    <link rel="stylesheet" href="../css/log_register.css" type="text/css"/>

</head>
<body>

<div id="register_form">
    <form method="post">
        Nickname:<br>
        <input type="text" name="nick" title="Nikname" value="<?php
        if (isset($_SESSION['fr_nick'])) {
            echo $_SESSION['fr_nick'];
            unset($_SESSION['fr_nick']);
        }
        ?>"><br>
        <?php
        if (isset($_SESSION['e_nick'])) {
            echo '<div class="error">' . $_SESSION['e_nick'] . '</div>';
            unset($_SESSION['e_nick']);
        }
        ?>
        E-mail:<br>
        <input type="text" name="email" title="E-mail" value="<?php
        if (isset($_SESSION['fr_email'])) {
            echo $_SESSION['fr_email'];
            unset($_SESSION['fr_email']);
        }
        ?>"><br>
        <?php
        if (isset($_SESSION['e_email'])) {
            echo '<div class="error">' . $_SESSION['e_email'] . '</div>';
            unset($_SESSION['e_email']);
        }
        ?>
        Hasło:<br>
        <input type="password" name="password" title="Hasło" value="<?php
        if (isset($_SESSION['fr_password'])) {
            echo $_SESSION['fr_password'];
            unset($_SESSION['fr_password']);
        }
        ?>"><br>
        <?php
        if (isset($_SESSION['e_password'])) {
            echo '<div class="error">' . $_SESSION['e_password'] . '</div>';
            unset($_SESSION['e_password']);
        }
        ?>
        Powtórz hasło:<br>
        <input type="password" name="password_repeat" title="Powtórz hasło" value="<?php
        if (isset($_SESSION['fr_password_repeat'])) {
            echo $_SESSION['fr_password_repeat'];
            unset($_SESSION['fr_password_repeat']);
        }
        ?>"><br><br>
        <?php
        if (isset($_SESSION['e_password_repeat'])) {
            echo '<div class="error">' . $_SESSION['e_password_repeat'] . '</div>';
            unset($_SESSION['e_password_repeat']);
        }
        ?>
        <label id="check_box_label_regulations">
            <input type="checkbox" name="regulations" title=""<?php
            if (isset($_SESSION['fr_regulations'])) {
                echo "checked";
                unset($_SESSION['fr_regulations']);
            }
            ?>>
            Akceptuje
        </label>
        <a href="../regulations/regulations.html" target="_blank">regulamin</a><br>
        <?php
        if (isset($_SESSION['e_regulations'])) {
            echo '<div class="error">' . $_SESSION['e_regulations'] . '</div>';
            unset($_SESSION['e_regulations']);
        }
        ?>
        <br>
        <div class="g-recaptcha" data-sitekey="6Ldo-hYTAAAAAJd0ZcsCIe4ptOmH3PEMvwExrIL3"></div>
        <?php
        if (isset($_SESSION['e_recaptcha'])) {
            echo '<div class="error">' . $_SESSION['e_recaptcha'] . '</div>';
            unset($_SESSION['e_recaptcha']);
        }
        ?>
        <br>
        <input type="submit" value="Zarejestruj się">

    </form>
</div>
</body>

</html>