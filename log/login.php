<?php
    include_once '../game/php_class/Game_container.php';
    include_once '../game/php_class/Kernel.php';
    @session_start();

    if(!isset($_POST['login']) && !isset($_POST['password'])){
        header('Location: ../index.php');
        exit();
    }

    //implementacja bazy MySQL
    require_once "connect.php";
    $conection = @new mysqli($host,$db_user,$db_password,$db_name);

    //sprawdzenie czy nawiązano połączenie, udane połączenie == 0
    if($conection->connect_errno != 0){
        echo "ERROR".$conection->connect_errno;
    }
    else {
        $login = $_POST['login'];
        $password = $_POST['password'];

        $login = htmlentities($login, ENT_QUOTES,"UTF-8");

        if($result = $conection->query(sprintf("SELECT * FROM users WHERE user='%s'",
            mysqli_real_escape_string($conection, $login))))
        {

            $number_of_users = $result->num_rows;
            if($number_of_users > 0){
                $row = $result->fetch_assoc();
                if(password_verify($password, $row['password'])) {
                    $_SESSION['log_in'] = true;
                    $_SESSION['id'] = $row['id'];
                    $_SESSION['user'] = $row['user'];
                    $_SESSION['email'] = $row['email'];
                    $_SESSION['wood'] = $row['wood'];
                    $_SESSION['stone'] = $row['stone'];
                    $_SESSION['bones'] = $row['bones'];
                    $_SESSION['grass'] = $row['grass'];
                    $_SESSION['food'] = $row['food'];
                    $_SESSION['points'] = $row['points'];
                    $_SESSION['last_login'] = $row['last_login'];

                    $game_container = new Game_container;
                    $game_container->setWood($_SESSION['wood']);
                    $game_container->setStone($_SESSION['stone']);
                    $game_container->setBones($_SESSION['bones']);
                    $game_container->setGrass($_SESSION['grass']);
                    $game_container->setFood($_SESSION['food']);
                    $game_container->setPoints($_SESSION['points']);
                    $game_container->setLastLogin($_SESSION['last_login']);

                    //wstępne obliczanie ilości materiałów i bonusłów związanych z levelem budynków
                    $_SESSION['game_container'] = serialize($game_container);
                    $kernel = new Kernel;
                    $kernel->calculate_size_of_warehouse();
                    $kernel->number_of_materials_when_offline();

                    //wyślij dane na serwer
                    $kernel->send_to_base($host,$db_user,$db_password,$db_name);

                    unset($_SESSION['error']);
                    $result->close();
                    header("Location: ../game/main_menu.php");
                } else{
                    $_SESSION['error'] = '<p style="color: red; font-weight: 600">Nieprawidłowy login lub hasło</p>';
                    header('Location: ../index.php');
                }
            }else {
                $_SESSION['error'] = '<p style="color: red; font-weight: 600">Nieprawidłowy login lub hasło</p>';
                header('Location: ../index.php');
            }

        }
        $conection->close();
    }

?>
