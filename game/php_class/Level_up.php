<?php
/**
* 
*/
class Level_up
{
	private $time = 200;

	private $wood = 100;
	private $stone = 100;
	private $grass = 100;
	private $bones = 100;
	private $food = 100;

	private $points = 5;

    /**
     * @return mixed
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @param mixed $time
     */
    public function setTime($time)
    {
        $this->time = $time;
    }

    /**
     * @return mixed
     */
    public function getWood()
    {
        return $this->wood;
    }

    /**
     * @param mixed $wood
     */
    public function setWood($wood)
    {
        $this->wood = $wood;
    }

    /**
     * @return mixed
     */
    public function getStone()
    {
        return $this->stone;
    }

    /**
     * @param mixed $stone
     */
    public function setStone($stone)
    {
        $this->stone = $stone;
    }

    /**
     * @return mixed
     */
    public function getGrass()
    {
        return $this->grass;
    }

    /**
     * @param mixed $grass
     */
    public function setGrass($grass)
    {
        $this->grass = $grass;
    }

    /**
     * @return mixed
     */
    public function getBones()
    {
        return $this->bones;
    }

    /**
     * @param mixed $bones
     */
    public function setBones($bones)
    {
        $this->bones = $bones;
    }

    /**
     * @return mixed
     */
    public function getFood()
    {
        return $this->food;
    }

    /**
     * @param mixed $food
     */
    public function setFood($food)
    {
        $this->food = $food;
    }

    /**
     * @return mixed
     */
    public function getPoints()
    {
        return $this->points;
    }

    /**
     * @param mixed $points
     */
    public function setPoints($points)
    {
        $this->points = $points;
    }

}


?>