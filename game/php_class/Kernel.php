<?php

require_once 'Game_container.php';
session_start();

class Kernel
{
    private function increase_of_material_per_second($lvl_of_building, $material, Game_container $game_container)
    {
        $tmp = 1;
        switch ($material) {
            case 'wood':
                $tmp = pow(1.5, $lvl_of_building);
                $game_container->setWoodPerSixtySecond($tmp);
                break;
            case 'stone':
                $tmp = pow(1.4, $lvl_of_building);
                $game_container->setStonePerSixtySecond($tmp);
                break;
            case 'bones':
                $tmp = pow(1.2, $lvl_of_building);
                $game_container->setBonesPerSixtySecond($tmp);
                break;
            case 'grass':
                $tmp = pow(1.3, $lvl_of_building);
                $game_container->setGrassPerSixtySecond($tmp);
                break;
            case 'food':
                $tmp = pow(1.3, $lvl_of_building);
                $game_container->setFoodPerSixtySecond($tmp);
                break;
        }
        return round($tmp);
    }

    function calculate_size_of_warehouse()
    {
        if (isset($_SESSION['game_container'])) {
            $game_container = unserialize($_SESSION['game_container']);
            //wood
            $wood_tmp = $game_container->getWoodcutterWarehouseLvl() * 500 + pow(1.5, $game_container->getWoodcutterWarehouseLvl()) * 100;
            $game_container->setWoodMaxCapacityOfWarehouse(round($wood_tmp));
            //stone
            $stone_tmp = $game_container->getStonemasonWarehouseLvl() * 500 + pow(1.5, $game_container->getStonemasonWarehouseLvl()) * 100;
            $game_container->setStoneMaxCapacityOfWarehouse(round($stone_tmp));
            //bones
            $bones_tmp = $game_container->getBonesCollectorWarehouseLvl() * 500 + pow(1.5, $game_container->getBonesCollectorWarehouseLvl()) * 100;
            $game_container->setBonesMaxCapacityOfWarehouse(round($bones_tmp));
            //grass
            $grass_tmp = $game_container->getGrassCollectorWarehouseLvl() * 500 + pow(1.5, $game_container->getGrassCollectorWarehouseLvl()) * 100;
            $game_container->setGrassMaxCapacityOfWarehouse(round($grass_tmp));
            //food
            $food_tmp = $game_container->getFruitPickerWarehouseLvl() * 500 + pow(1.5, $game_container->getFruitPickerWarehouseLvl()) * 100;
            $game_container->setFoodMaxCapacityOfWarehouse(round($food_tmp));

            $_SESSION['game_container'] = serialize($game_container);
        }
    }

    function number_of_materials_when_offline()
    {
        if (isset($_SESSION['game_container'])) {
            $game_container = unserialize($_SESSION['game_container']);
            $time_offline = time() - $game_container->getLastLogin();

            //wood
            $tmp_wood = $_SESSION['wood'] + (round($time_offline / 60)) * $this->increase_of_material_per_second($game_container->getWoodcutterHutLvl(), 'wood', $game_container);
            if ($tmp_wood > $game_container->getWoodMaxCapacityOfWarehouse())
                $game_container->setWood($game_container->getWoodMaxCapacityOfWarehouse());
            else
                $game_container->setWood($tmp_wood);
            //stone
            $tmp_stone = $_SESSION['stone'] + (round($time_offline / 60)) * $this->increase_of_material_per_second($game_container->getStonemasonHutLvl(), 'stone', $game_container);
            if ($tmp_stone > $game_container->getStoneMaxCapacityOfWarehouse())
                $game_container->setStone($game_container->getStoneMaxCapacityOfWarehouse());
            else
                $game_container->setStone($tmp_stone);
            //bones
            $tmp_bones = $_SESSION['bones'] + (round($time_offline / 60)) * $this->increase_of_material_per_second($game_container->getBonesCollectorHutLvl(), 'bones', $game_container);
            if ($tmp_bones > $game_container->getBonesMaxCapacityOfWarehouse())
                $game_container->setBones($game_container->getBonesMaxCapacityOfWarehouse());
            else
                $game_container->setBones($tmp_bones);
            //grass
            $tmp_grass = $_SESSION['grass'] + (round($time_offline / 60)) * $this->increase_of_material_per_second($game_container->getGrassCollectorHutLvl(), 'grass', $game_container);
            if ($tmp_grass > $game_container->getGrassMaxCapacityOfWarehouse())
                $game_container->setGrass($game_container->getGrassMaxCapacityOfWarehouse());
            else
                $game_container->setGrass($tmp_grass);
            //food
            $tmp_food = $_SESSION['food'] + (round($time_offline / 60)) * $this->increase_of_material_per_second($game_container->getFruitPickerHutLvl(), 'food', $game_container);
            if ($tmp_food > $game_container->getFoodMaxCapacityOfWarehouse())
                $game_container->setFood($game_container->getFoodMaxCapacityOfWarehouse());
            else
                $game_container->setFood($tmp_food);

            $_SESSION['game_container'] = serialize($game_container);
        }
    }

    function calculate_time_to_next_level(){
        
    }

    function send_to_base($host, $db_user, $db_password, $db_name)
    {
        $connection = @new mysqli($host, $db_user, $db_password, $db_name);
        if (isset($_SESSION['game_container'])) {
            $game_container = unserialize($_SESSION['game_container']);
            if ($connection->connect_errno != 0) {
                echo "ERROR" . $connection->connect_errno;
            } else {
                $login = $_SESSION['user'];
                if ($result = $connection->query(sprintf("SELECT * FROM users WHERE user='%s'",
                    mysqli_real_escape_string($connection, $login)))
                ) {
                    $number_of_users = $result->num_rows;
                    if ($number_of_users > 0) {
                        $row = $result->fetch_assoc();
                        $id = $row['id'];
                        $wood = $game_container->getWood();
                        $stone = $game_container->getStone();
                        $bones = $game_container->getBones();
                        $grass = $game_container->getGrass();
                        $food = $game_container->getFood();
                        $points = $game_container->getPoints();
                        $time = time();                        

                        $connection->query("UPDATE users SET wood='$wood', stone='$stone', bones='$bones', grass='$grass', food='$food', points='$points',last_login='$time' WHERE id='$id'");
                        $connection->close();
                    }
                }
            }
        }
    }
}

