<?php
require_once 'Level_up.php';

class Game_container
{
    //Materials
    private $wood;
    private $stone;
    private $bones;
    private $grass;
    private $food;

    //points
    private $last_login;
    private $points;

    //level of buildings
    private $woodcutter_hut_lvl = 1;
    private $woodcutter_hut_lvl_up;

    private $stonemason_hut_lvl = 1;
    private $stonemason_hut_lvl_up;

    private $bones_collector_hut_lvl = 1;
    private $bones_collector_hut_lvl_up;

    private $grass_collector_hut_lvl = 1;
    private $grass_collector_hut_lvl_up;

    private $fruit_picker_hut_lvl = 1;
    private $fruit_picker_hut_lvl_up;

    private $woodcutter_warehouse_lvl = 1;
    private $stonemason_warehouse_lvl = 1;
    private $bones_collector_warehouse_lvl = 1;
    private $grass_collector_warehouse_lvl = 1;
    private $fruit_picker_warehouse_lvl = 1;

    private $center_of_city_lvl = 1;
    private $barrack_lvl = 1;
    private $hunter_animals_hut_lvl = 1;
    private $lab_lvl = 1;
    private $builder_hut_lvl = 1;

    //max_capacity_of_warehouse
    private $wood_max_capacity_of_warehouse;
    private $stone_max_capacity_of_warehouse;
    private $bones_max_capacity_of_warehouse;
    private $grass_max_capacity_of_warehouse;
    private $food_max_capacity_of_warehouse;

    //materials per 1 minute
    private $wood_per_sixty_second;
    private $stone_per_sixty_second;
    private $bones_per_sixty_second;
    private $grass_per_sixty_second;
    private $food_per_sixty_second;

    /**
     * Game_container constructor.
     */
    public function __construct()
    {
        $this->stonemason_hut_lvl_up = $this->woodcutter_hut_lvl_up = new Level_up();
        $this->bones_collector_hut_lvl_up = new Level_up();
        $this->grass_collector_hut_lvl_up = new Level_up();
        $this->fruit_picker_hut_lvl_up = new Level_up();
    }

    //level of reserach

    //number of tropos


    /**
     * @return mixed
     */
    public function getWood()
    {
        return $this->wood;
    }

    /**
     * @param mixed $wood
     */
    public function setWood($wood)
    {
        $this->wood = $wood;
    }

    /**
     * @return mixed
     */
    public function getStone()
    {
        return $this->stone;
    }

    /**
     * @param mixed $stone
     */
    public function setStone($stone)
    {
        $this->stone = $stone;
    }

    /**
     * @return mixed
     */
    public function getBones()
    {
        return $this->bones;
    }

    /**
     * @param mixed $bones
     */
    public function setBones($bones)
    {
        $this->bones = $bones;
    }

    /**
     * @return mixed
     */
    public function getGrass()
    {
        return $this->grass;
    }

    /**
     * @param mixed $grass
     */
    public function setGrass($grass)
    {
        $this->grass = $grass;
    }

    /**
     * @return mixed
     */
    public function getFood()
    {
        return $this->food;
    }

    /**
     * @param mixed $food
     */
    public function setFood($food)
    {
        $this->food = $food;
    }

    /**
     * @return mixed
     */
    public function getLastLogin()
    {
        return $this->last_login;
    }

    /**
     * @param mixed $last_login
     */
    public function setLastLogin($last_login)
    {
        $this->last_login = $last_login;
    }

    /**
     * @return mixed
     */
    public function getPoints()
    {
        return $this->points;
    }

    /**
     * @param mixed $points
     */
    public function setPoints($points)
    {
        $this->points = $points;
    }

    /**
     * @return mixed
     */
    public function getWoodcutterHutLvl()
    {
        return $this->woodcutter_hut_lvl;
    }

    /**
     * @param mixed $woodcutter_hut_lvl
     */
    public function setWoodcutterHutLvl($woodcutter_hut_lvl)
    {
        $this->woodcutter_hut_lvl = $woodcutter_hut_lvl;
    }

    /**
     * @return mixed
     */
    public function getStonemasonHutLvl()
    {
        return $this->stonemason_hut_lvl;
    }

    /**
     * @param mixed $stonemason_hut_lvl
     */
    public function setStonemasonHutLvl($stonemason_hut_lvl)
    {
        $this->stonemason_hut_lvl = $stonemason_hut_lvl;
    }

    /**
     * @return mixed
     */
    public function getBonesCollectorHutLvl()
    {
        return $this->bones_collector_hut_lvl;
    }

    /**
     * @param mixed $bones_collector_hut_lvl
     */
    public function setBonesCollectorHutLvl($bones_collector_hut_lvl)
    {
        $this->bones_collector_hut_lvl = $bones_collector_hut_lvl;
    }

    /**
     * @return mixed
     */
    public function getGrassCollectorHutLvl()
    {
        return $this->grass_collector_hut_lvl;
    }

    /**
     * @param mixed $grass_collector_hut_lvl
     */
    public function setGrassCollectorHutLvl($grass_collector_hut_lvl)
    {
        $this->grass_collector_hut_lvl = $grass_collector_hut_lvl;
    }

    /**
     * @return mixed
     */
    public function getFruitPickerHutLvl()
    {
        return $this->fruit_picker_hut_lvl;
    }

    /**
     * @param mixed $fruit_picker_hut_lvl
     */
    public function setFruitPickerHutLvl($fruit_picker_hut_lvl)
    {
        $this->fruit_picker_hut_lvl = $fruit_picker_hut_lvl;
    }

    /**
     * @return mixed
     */
    public function getWoodcutterWarehouseLvl()
    {
        return $this->woodcutter_warehouse_lvl;
    }

    /**
     * @param mixed $woodcutter_warehouse_lvl
     */
    public function setWoodcutterWarehouseLvl($woodcutter_warehouse_lvl)
    {
        $this->woodcutter_warehouse_lvl = $woodcutter_warehouse_lvl;
    }

    /**
     * @return mixed
     */
    public function getStonemasonWarehouseLvl()
    {
        return $this->stonemason_warehouse_lvl;
    }

    /**
     * @param mixed $stonemason_warehouse_lvl
     */
    public function setStonemasonWarehouseLvl($stonemason_warehouse_lvl)
    {
        $this->stonemason_warehouse_lvl = $stonemason_warehouse_lvl;
    }

    /**
     * @return mixed
     */
    public function getBonesCollectorWarehouseLvl()
    {
        return $this->bones_collector_warehouse_lvl;
    }

    /**
     * @param mixed $bones_collector_warehouse_lvl
     */
    public function setBonesCollectorWarehouseLvl($bones_collector_warehouse_lvl)
    {
        $this->bones_collector_warehouse_lvl = $bones_collector_warehouse_lvl;
    }

    /**
     * @return mixed
     */
    public function getGrassCollectorWarehouseLvl()
    {
        return $this->grass_collector_warehouse_lvl;
    }

    /**
     * @param mixed $grass_collector_warehouse_lvl
     */
    public function setGrassCollectorWarehouseLvl($grass_collector_warehouse_lvl)
    {
        $this->grass_collector_warehouse_lvl = $grass_collector_warehouse_lvl;
    }

    /**
     * @return mixed
     */
    public function getFruitPickerWarehouseLvl()
    {
        return $this->fruit_picker_warehouse_lvl;
    }

    /**
     * @param mixed $fruit_picker_warehouse_lvl
     */
    public function setFruitPickerWarehouseLvl($fruit_picker_warehouse_lvl)
    {
        $this->fruit_picker_warehouse_lvl = $fruit_picker_warehouse_lvl;
    }

    /**
     * @return mixed
     */
    public function getCenterOfCityLvl()
    {
        return $this->center_of_city_lvl;
    }

    /**
     * @param mixed $center_of_city_lvl
     */
    public function setCenterOfCityLvl($center_of_city_lvl)
    {
        $this->center_of_city_lvl = $center_of_city_lvl;
    }

    /**
     * @return mixed
     */
    public function getBarrackLvl()
    {
        return $this->barrack_lvl;
    }

    /**
     * @param mixed $barrack_lvl
     */
    public function setBarrackLvl($barrack_lvl)
    {
        $this->barrack_lvl = $barrack_lvl;
    }

    /**
     * @return mixed
     */
    public function getHunterAnimalsHutLvl()
    {
        return $this->hunter_animals_hut_lvl;
    }

    /**
     * @param mixed $hunter_animals_hut_lvl
     */
    public function setHunterAnimalsHutLvl($hunter_animals_hut_lvl)
    {
        $this->hunter_animals_hut_lvl = $hunter_animals_hut_lvl;
    }

    /**
     * @return mixed
     */
    public function getLabLvl()
    {
        return $this->lab_lvl;
    }

    /**
     * @param mixed $lab_lvl
     */
    public function setLabLvl($lab_lvl)
    {
        $this->lab_lvl = $lab_lvl;
    }

    /**
     * @return mixed
     */
    public function getBuilderHutLvl()
    {
        return $this->builder_hut_lvl;
    }

    /**
     * @param mixed $builder_hut_lvl
     */
    public function setBuilderHutLvl($builder_hut_lvl)
    {
        $this->builder_hut_lvl = $builder_hut_lvl;
    }

    /**
     * @return mixed
     */
    public function getWoodMaxCapacityOfWarehouse()
    {
        return $this->wood_max_capacity_of_warehouse;
    }

    /**
     * @param mixed $wood_max_capacity_of_warehouse
     */
    public function setWoodMaxCapacityOfWarehouse($wood_max_capacity_of_warehouse)
    {
        $this->wood_max_capacity_of_warehouse = $wood_max_capacity_of_warehouse;
    }

    /**
     * @return mixed
     */
    public function getStoneMaxCapacityOfWarehouse()
    {
        return $this->stone_max_capacity_of_warehouse;
    }

    /**
     * @param mixed $stone_max_capacity_of_warehouse
     */
    public function setStoneMaxCapacityOfWarehouse($stone_max_capacity_of_warehouse)
    {
        $this->stone_max_capacity_of_warehouse = $stone_max_capacity_of_warehouse;
    }

    /**
     * @return mixed
     */
    public function getBonesMaxCapacityOfWarehouse()
    {
        return $this->bones_max_capacity_of_warehouse;
    }

    /**
     * @param mixed $bones_max_capacity_of_warehouse
     */
    public function setBonesMaxCapacityOfWarehouse($bones_max_capacity_of_warehouse)
    {
        $this->bones_max_capacity_of_warehouse = $bones_max_capacity_of_warehouse;
    }

    /**
     * @return mixed
     */
    public function getGrassMaxCapacityOfWarehouse()
    {
        return $this->grass_max_capacity_of_warehouse;
    }

    /**
     * @param mixed $grass_max_capacity_of_warehouse
     */
    public function setGrassMaxCapacityOfWarehouse($grass_max_capacity_of_warehouse)
    {
        $this->grass_max_capacity_of_warehouse = $grass_max_capacity_of_warehouse;
    }

    /**
     * @return mixed
     */
    public function getFoodMaxCapacityOfWarehouse()
    {
        return $this->food_max_capacity_of_warehouse;
    }

    /**
     * @param mixed $food_max_capacity_of_warehouse
     */
    public function setFoodMaxCapacityOfWarehouse($food_max_capacity_of_warehouse)
    {
        $this->food_max_capacity_of_warehouse = $food_max_capacity_of_warehouse;
    }

    /**
     * @return mixed
     */
    public function getWoodPerSixtySecond()
    {
        return $this->wood_per_sixty_second;
    }

    /**
     * @param mixed $wood_per_sixty_second
     */
    public function setWoodPerSixtySecond($wood_per_sixty_second)
    {
        $this->wood_per_sixty_second = $wood_per_sixty_second;
    }

    /**
     * @return mixed
     */
    public function getStonePerSixtySecond()
    {
        return $this->stone_per_sixty_second;
    }

    /**
     * @param mixed $stone_per_sixty_second
     */
    public function setStonePerSixtySecond($stone_per_sixty_second)
    {
        $this->stone_per_sixty_second = $stone_per_sixty_second;
    }

    /**
     * @return mixed
     */
    public function getBonesPerSixtySecond()
    {
        return $this->bones_per_sixty_second;
    }

    /**
     * @param mixed $bones_per_sixty_second
     */
    public function setBonesPerSixtySecond($bones_per_sixty_second)
    {
        $this->bones_per_sixty_second = $bones_per_sixty_second;
    }

    /**
     * @return mixed
     */
    public function getGrassPerSixtySecond()
    {
        return $this->grass_per_sixty_second;
    }

    /**
     * @param mixed $grass_per_sixty_second
     */
    public function setGrassPerSixtySecond($grass_per_sixty_second)
    {
        $this->grass_per_sixty_second = $grass_per_sixty_second;
    }

    /**
     * @return mixed
     */
    public function getFoodPerSixtySecond()
    {
        return $this->food_per_sixty_second;
    }

    /**
     * @param mixed $food_per_sixty_second
     */
    public function setFoodPerSixtySecond($food_per_sixty_second)
    {
        $this->food_per_sixty_second = $food_per_sixty_second;
    }

    /**
     * @return Level_up
     */
    public function getWoodcutterHutLvlUp()
    {
        return $this->woodcutter_hut_lvl_up;
    }

    /**
     * @param Level_up $woodcutter_hut_lvl_up
     */
    public function setWoodcutterHutLvlUp($woodcutter_hut_lvl_up)
    {
        $this->woodcutter_hut_lvl_up = $woodcutter_hut_lvl_up;
    }

    /**
     * @return Level_up
     */
    public function getStonemasonHutLvlUp()
    {
        return $this->stonemason_hut_lvl_up;
    }

    /**
     * @param Level_up $stonemason_hut_lvl_up
     */
    public function setStonemasonHutLvlUp($stonemason_hut_lvl_up)
    {
        $this->stonemason_hut_lvl_up = $stonemason_hut_lvl_up;
    }

    /**
     * @return Level_up
     */
    public function getBonesCollectorHutLvlUp()
    {
        return $this->bones_collector_hut_lvl_up;
    }

    /**
     * @param Level_up $bones_collector_hut_lvl_up
     */
    public function setBonesCollectorHutLvlUp($bones_collector_hut_lvl_up)
    {
        $this->bones_collector_hut_lvl_up = $bones_collector_hut_lvl_up;
    }

    /**
     * @return Level_up
     */
    public function getGrassCollectorHutLvlUp()
    {
        return $this->grass_collector_hut_lvl_up;
    }

    /**
     * @param Level_up $grass_collector_hut_lvl_up
     */
    public function setGrassCollectorHutLvlUp($grass_collector_hut_lvl_up)
    {
        $this->grass_collector_hut_lvl_up = $grass_collector_hut_lvl_up;
    }

    /**
     * @return Level_up
     */
    public function getFruitPickerHutLvlUp()
    {
        return $this->fruit_picker_hut_lvl_up;
    }

    /**
     * @param Level_up $fruit_picker_hut_lvl_up
     */
    public function setFruitPickerHutLvlUp($fruit_picker_hut_lvl_up)
    {
        $this->fruit_picker_hut_lvl_up = $fruit_picker_hut_lvl_up;
    }



}