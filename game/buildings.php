<?php
require_once '../game/php_class/Game_container.php';
require_once '../game/php_class/Kernel.php';
require_once '../log/connect.php';

//session_start();
if (!isset($_SESSION['log_in'])) {
    header('Location: ../index.php');
    exit();
}
//obliczenie wartości surowców
$game_container = unserialize($_SESSION['game_container']);
$kernel = new Kernel;
$kernel->number_of_materials_when_offline();
//$kernel->send_to_base($host,$db_user,$db_password,$db_name);
?>

<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <meta name="discriptions" content="">
    <meta name="author" content="Cezary Wolszczak">
    <meta name="keywords" content="">
    <meta http-equiv="x-ua-compatible" content="IE=edge"/>
    <title>Plejstocen - Menu główne</title>
    <link rel="icon" href="">

    <script src="../js/page_items.js" type="text/javascript"></script>
    <script src="../js/jquery-2.2.0.min.js" type="text/javascript"></script>
    <script src="../js/jquery-ui.min.js" type="text/javascript"></script>

    <link rel="stylesheet" href="../css/style.css" type="text/css"/>
    <link rel="stylesheet" href="../css/game.css" type="text/css"/>
    <link rel="stylesheet" href="../css/jquery-ui.min.css" type="text/css"/>

    <script> tooltips(".raw_materials_items"); </script>

</head>

<body onload="refresh()">

<div id="second_menu">
    <div class="second_menu_items"><?php
        echo "Gracz: &nbsp;&nbsp;" . $_SESSION['user'];
        ?>
    </div>
    <div class="second_menu_items" style="width: 42%;">
        &nbsp;
    </div>
    <div class="second_menu_items" id="timer"></div>
    <div class="second_menu_items">
        <a href="#">Ustawienia</a>
    </div>
    <div class="second_menu_items">
        <a href="../log/logout.php">Wyloguj</a>
    </div>
    <div style="clear: both"></div>
</div>

<div id="raw_materials">
    <div class="raw_materials_items" title="
    <?php
    echo "<span class='tooltip_materials_1'>Drewno</span>
    <span class='tooltip_materials_2'>
        <br>Dostępne surowce: ".number_format($game_container->getWood(), 0, ' ', '.').
        "<br>Pojemność magazynu: ".number_format($game_container->getWoodMaxCapacityOfWarehouse(), 0, ' ', '.').
        "<br>Produkcja na minute: ".number_format($game_container->getWoodPerSixtySecond(), 0, ' ', '.').
    "</span>";
    ?>">
        <?php
        echo " Drewno: " . number_format($game_container->getWood(), 0, ' ', '.');
        ?></div>

    <div class="raw_materials_items" title="
    <?php
    echo "<span class='tooltip_materials_1'>Kamień</span>
    <span class='tooltip_materials_2'>
        <br>Dostępne surowce: ".number_format($game_container->getStone(), 0, ' ', '.').
        "<br>Pojemność magazynu: ".number_format($game_container->getStoneMaxCapacityOfWarehouse(), 0, ' ', '.').
        "<br>Produkcja na minute: ".number_format($game_container->getStonePerSixtySecond(), 0, ' ', '.').
        "</span>";
    ?>">
        <?php
        echo " Kamień: " . number_format($game_container->getStone(), 0, ' ', '.');
        ?></div>

    <div class="raw_materials_items" title="
    <?php
    echo "<span class='tooltip_materials_1'>Kości</span>
    <span class='tooltip_materials_2'>
        <br>Dostępne surowce: ".number_format($game_container->getBones(), 0, ' ', '.').
        "<br>Pojemność magazynu: ".number_format($game_container->getBonesMaxCapacityOfWarehouse(), 0, ' ', '.').
        "<br>Produkcja na minute: ".number_format($game_container->getBonesPerSixtySecond(), 0, ' ', '.').
        "</span>";
    ?>">
        <?php
        echo " Kości: " . number_format($game_container->getBones(), 0, ' ', '.');
        ?></div>

    <div class="raw_materials_items" title="
    <?php
    echo "<span class='tooltip_materials_1'>Trawa</span>
    <span class='tooltip_materials_2'>
        <br>Dostępne surowce: ".number_format($game_container->getGrass(), 0, ' ', '.').
        "<br>Pojemność magazynu: ".number_format($game_container->getGrassMaxCapacityOfWarehouse(), 0, ' ', '.').
        "<br>Produkcja na minute: ".number_format($game_container->getGrassPerSixtySecond(), 0, ' ', '.').
        "</span>";
    ?>">
        <?php
        echo " Trawa: " . number_format($game_container->getGrass(), 0, ' ', '.');
        ?></div>

    <div class="raw_materials_items" title="
    <?php
    echo "<span class='tooltip_materials_1'>Żywność</span>
    <span class='tooltip_materials_2'>
        <br>Dostępne surowce: ".number_format($game_container->getFood(), 0, ' ', '.').
        "<br>Pojemność magazynu: ".number_format($game_container->getFoodMaxCapacityOfWarehouse(), 0, ' ', '.').
        "<br>Produkcja na minute: ".number_format($game_container->getFoodPerSixtySecond(), 0, ' ', '.').
        "</span>";
    ?>">
        <?php
        echo " Żywność: " . number_format($game_container->getFood(), 0, ' ', '.');
        ?></div>

    <div style="clear: both"></div>

</div>


<div id="container">
    <div id="left_main_menu">
        <div style="text-align: left; margin-bottom: 50px; font-size: 30px;">
            Plejstocen
        </div>
        <table>
            <tr>
                <td>
                    <a href="main_menu.php">Menu główne</a>
                </td>
            </tr>
            <tr>
                <td>
                    <a href="buildings.php">Surowce</a>
                </td>
            </tr>
            <tr>
                <td>
                    <a href="reserach.php">Badania</a>
                </td>
            </tr>
            <tr>
                <td>
                    <a href="barracks.php">Koszary</a>
                </td>
            </tr>
            <tr>
                <td>
                    <a href="alliance.php">Sojusz</a>
                </td>
            </tr>
            <tr>
                <td>
                    <a href="shop.php">Sklep</a>
                </td>
            </tr>
            <tr>
                <td>
                    <a href="ranking.php">Ranking</a>
                </td>
            </tr>                                    
        </table>
    </div>
    <div id="materials_level_up_button_container">
        <div class="materials_level_up_button" id="m1" onclick="init_progress_bar(<?php echo $game_container->getWoodcutterHutLvlUp()->getTime() ?>, 10, 1)">Chata drwala<br><br>lvl <?php echo $game_container->getWoodcutterHutLvl(); ?>
           <?php
                echo "<div class='pbar_outerdiv'>
                    <div class='pbar_innerdiv'></div>
                    <div class='pbar_innertext'></div>
                </div>";
           ?>
        </div>
        <div class="materials_level_up_button" onclick="init_progress_bar(10, 10, 1)">chata kamieniarza<br><br>lvl <?php echo $game_container->getStonemasonHutLvl(); ?>
            <div class="pbar_outerdiv">
                <div class="pbar_innerdiv"></div>
                <div class="pbar_innertext"></div>
            </div>
        </div>
        <div class="materials_level_up_button">Chata zbieracza kości<br>lvl <?php echo $game_container->getBonesCollectorHutLvl(); ?>
        </div>
        <div class="materials_level_up_button">Chata zbieracza trawy<br>lvl <?php echo $game_container->getGrassCollectorHutLvl(); ?>
        </div>
        <div class="materials_level_up_button">Chata zbieracza pożywienia<br>lvl <?php echo $game_container->getFruitPickerHutLvl(); ?>
        </div>
        <div class="materials_level_up_button">Magazyn drewna<br><br>lvl <?php echo $game_container->getWoodcutterWarehouseLvl(); ?>
        </div>
        <div class="materials_level_up_button">Magazyn kamienia<br><br>lvl <?php echo $game_container->getStonemasonWarehouseLvl(); ?>
        </div>
        <div class="materials_level_up_button">Magazyn kości<br><br>lvl <?php echo $game_container->getBonesCollectorWarehouseLvl(); ?>
        </div>
        <div class="materials_level_up_button">Magazyn trawy<br><br>lvl <?php echo $game_container->getGrassCollectorWarehouseLvl(); ?>
        </div>
        <div class="materials_level_up_button">Magazyn żywności<br><br>lvl <?php echo $game_container->getFruitPickerWarehouseLvl(); ?>
        </div>
        <div style="clear: both;"></div>
    </div>

    <div style="clear: both;"></div>
</div>


<div id="footer">
    Copyright 2016 &copy; Cezary Wolszczak
</div>

</body>

</html>